Cedric Development
===========

To get started with any project firstly you need to initialize the repository by

    repo init -u git://github.com/xyz.git -b xyz

Then go get the local manifests

    Download xyz.xml 
    And lastly move it to *YOUR ROM DIRECTORY*/.repo/local_manifests

Then to sync up:

    repo sync

Then to build:

     cd <source-dir>; . build/envsetup.sh; lunch xyz_<device>-userdebug; brunch <device_name>


To get more familiar with [Git and Repo](https://source.android.com/source/using-repo.html).